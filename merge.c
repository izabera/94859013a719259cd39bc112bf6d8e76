#include <stdio.h>

#include "smartstring.h"

int main() {
  string a = save(str("foo")),
         b = save(str("bar")),
         c = save(str("baz")),
         d = save(str("bat"));
  string merged = save(merge(merge(a, b), merge(c, d)));
  print("final string", merged);
  print("a", a);
  print("b", b);
  print("c", c);
  print("d", d);
  print("last one", merge(merge(str("hello"), str(", ")), merge(str("world"), str("!"))));
  return 0;
}
